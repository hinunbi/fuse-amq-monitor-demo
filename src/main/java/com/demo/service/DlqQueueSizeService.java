package com.demo.service;

import org.apache.camel.Body;
import org.apache.camel.jsonpath.JsonPath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DlqQueueSizeService {

  private static final Logger logger = LoggerFactory.getLogger(DlqQueueSizeService.class);

  public Integer sum(@JsonPath("$.value.*.QueueSize") ArrayList queueSizes, @Body String body) {

    // logger.info("queueSizes : {}", queueSizes);

    Integer total = 0;
    for (Object o : queueSizes) {
      total += (Integer) o;
    }
    return total;
  }
}
